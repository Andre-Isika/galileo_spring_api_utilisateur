package fr.isika.projet4.galileo.services;

import java.util.Collection;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import fr.isika.projet4.galileo.models.Livre;


public class LivreDao implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String mongoId;
	

    public LivreDao(Long id, String mongoId) {
        this.id = id;
        this.mongoId = mongoId;

    }

    public static LivreDao build(Livre livre) {
    	
        return new LivreDao(
        		livre.getId(),
        		livre.getMongoId()
        );
    }
    
    
    public Long getId() {
        return id;
    }

    public String getMongoId() {
        return mongoId;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Stub de la méthode généré automatiquement
		return null;
	}

	@Override
	public String getPassword() {
		// TODO Stub de la méthode généré automatiquement
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Stub de la méthode généré automatiquement
		return null;
	}
	
	

}
