package fr.isika.projet4.galileo.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.projet4.galileo.models.Utilisateur;
import fr.isika.projet4.galileo.repository.UtilisateurRepository;


@Service
public class UtilisateurDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
    	
        Utilisateur utilisateur = utilisateurRepository.findByUsername(username)
                	.orElseThrow(() -> 
                        new UsernameNotFoundException("Le nom d'utilisateur " + username + " n'existe pas !")
        );

        return UtilisateurDao.build(utilisateur);
    }


}
