package fr.isika.projet4.galileo.services;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.isika.projet4.galileo.models.Utilisateur;


public class UtilisateurDao implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

    private String nom;
    
    private String prenom;

	private String username;

    private String email;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;
    
    private Collection<String> auteurs;
    
    private Collection<String> livres;

    public UtilisateurDao(Long id, String nom, String prenom,
			    		String username, String email, String password, 
			    		Collection<? extends GrantedAuthority> authorities, Collection<String> auteurs, Collection<String> livres) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
        this.auteurs = auteurs;
        this.livres = livres;
    }

    public static UtilisateurDao build(Utilisateur utilisateur) {
    	
        List<GrantedAuthority> authorities = utilisateur.getRoles().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());
        
        List<String> auteurs = utilisateur.getAuteurs().stream().map(auteur -> auteur.getMongoId()
                ).collect(Collectors.toList());
        
        List<String> livres = utilisateur.getLivres().stream().map(livre -> livre.getMongoId()
                ).collect(Collectors.toList());

        return new UtilisateurDao(
                utilisateur.getId(),
                utilisateur.getNom(),
                utilisateur.getPrenom(),
                utilisateur.getUsername(),
                utilisateur.getEmail(),
                utilisateur.getPassword(),
                authorities,
                auteurs,
                livres
        );
    }
    
    
    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }
    
    public String getPrenom() {
 		return prenom;
 	}

 	public void setPrenom(String prenom) {
 		this.prenom = prenom;
 	}

    public String getEmail() {
        return email;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }


    public Collection<String> getAuteurs() {
        return auteurs;
    }

    public Collection<String> getLivres() {
        return livres;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        UtilisateurDao utilisateur = (UtilisateurDao) o;
        return Objects.equals(id, utilisateur.id);
    }
	
	

}
