package fr.isika.projet4.galileo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GalileoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GalileoApplication.class, args);
	}

}
