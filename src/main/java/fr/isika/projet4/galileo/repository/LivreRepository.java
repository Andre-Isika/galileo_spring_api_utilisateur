package fr.isika.projet4.galileo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isika.projet4.galileo.models.Livre;


@Repository
public interface LivreRepository extends JpaRepository<Livre, Long>{
	
    Optional<Livre> findByMongoId(String id_mongo);
    Boolean existsByMongoId(String id_mong);
}
