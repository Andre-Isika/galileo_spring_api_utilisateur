package fr.isika.projet4.galileo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isika.projet4.galileo.models.Auteur;

@Repository
public interface AuteurRepository extends JpaRepository<Auteur, Long>{
	
    Optional<Auteur> findByMongoId(String id_mongo);
    Boolean existsByMongoId(String id_mongo);
}

