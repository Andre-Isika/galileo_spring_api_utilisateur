package fr.isika.projet4.galileo.message.reponse;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class JwtReponse {
	
    private String token;
    private String type = "Bearer";
    private String nom;
    private String prenom;
	private String username;
	private Collection<? extends GrantedAuthority> authorities;
//	private Collection<String> auteurs;
    private Collection<String> livres;


    public JwtReponse(String accessToken,Collection<String> livres, String nom, String prenom, String username, Collection<? extends GrantedAuthority> authorities) {
        this.token = accessToken;
        this.nom=nom;
        this.prenom = prenom;
        this.username = username;
        this.authorities = authorities;
        this.livres = livres;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public Collection<String> getLivres() {
		return livres;
	}

	public void setLivres(Collection<String> livres) {
		this.livres = livres;
	}

}
