package fr.isika.projet4.galileo.message.reponse;

import java.util.ArrayList;
import java.util.List;

public class AuteurReponse {
	
	List<String>auteurs= new ArrayList<String>();
	
	AuteurReponse(List<String> auteurs){
		
		this.auteurs= auteurs;
		
	}
	

	public AuteurReponse() {
		
	}

	public List<String> getAuteurs() {
		return auteurs;
	}

	public void setAuteurs(List<String> auteurs) {
		this.auteurs = auteurs;
	}
	
	
}
