package fr.isika.projet4.galileo.message.requete;

import javax.validation.constraints.NotBlank;

public class LivreLu {
	
    @NotBlank
    private String mongoId;
    
    @NotBlank
    private String username;

	public String getMongoId() {
		return mongoId;
	}

	public void setMongoId(String mongoId) {
		this.mongoId = mongoId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
    
    

}
