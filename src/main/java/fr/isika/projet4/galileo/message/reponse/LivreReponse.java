package fr.isika.projet4.galileo.message.reponse;
import java.util.List;
import java.util.ArrayList;

public class LivreReponse {

	List livres = new ArrayList<String>();
	
	public LivreReponse() {
		
	}

	public LivreReponse(List livres) {
		this.livres = livres;
	}

	public List getLivres() {
		return livres;
	}

	public void setLivres(List<String> livres) {
		this.livres = livres;
	}
	
}
