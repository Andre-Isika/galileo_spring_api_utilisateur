package fr.isika.projet4.galileo.message.requete;

import javax.validation.constraints.NotBlank;

public class MonAuteur {
	
	    @NotBlank
	    private String username;
	    
	    private String idAuteur;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getIdAuteur() {
			return idAuteur;
		}

		public void setIdAuteur(String idAuteur) {
			this.idAuteur = idAuteur;
		}

	    

}