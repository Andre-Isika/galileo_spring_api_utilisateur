package fr.isika.projet4.galileo.message.requete;

import javax.validation.constraints.NotBlank;


public class AjoutLivre {
	
	    @NotBlank
	    private String mongoId;

		public String getMongoId() {
			return mongoId;
		}

		public void setMongoId(String mongoId) {
			this.mongoId = mongoId;
		}


}
