package fr.isika.projet4.galileo.message.requete;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	
	@NotBlank
	String username;

	@JsonCreator
	public User(@JsonProperty("username") String username) {
		this.username = username;
	}

	public String getIdMongo() {
		return username;
	}

	public void setIdMongo(String username) {
		this.username = username;
	}
	
	

}
