package fr.isika.projet4.galileo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.projet4.galileo.message.reponse.AuteurReponse;
import fr.isika.projet4.galileo.message.reponse.JwtReponse;
import fr.isika.projet4.galileo.message.reponse.LivreReponse;
import fr.isika.projet4.galileo.message.reponse.ResponseMessage;
import fr.isika.projet4.galileo.message.requete.AjoutAuteur;
import fr.isika.projet4.galileo.message.requete.AjoutLivre;
import fr.isika.projet4.galileo.message.requete.LivreLu;
import fr.isika.projet4.galileo.message.requete.LoginForm;
import fr.isika.projet4.galileo.message.requete.MonAuteur;
import fr.isika.projet4.galileo.message.requete.SignUpForm;
import fr.isika.projet4.galileo.message.requete.User;
import fr.isika.projet4.galileo.models.Auteur;
import fr.isika.projet4.galileo.models.Livre;
import fr.isika.projet4.galileo.models.Role;
import fr.isika.projet4.galileo.models.RoleName;
import fr.isika.projet4.galileo.models.Utilisateur;
import fr.isika.projet4.galileo.repository.AuteurRepository;
import fr.isika.projet4.galileo.repository.LivreRepository;
import fr.isika.projet4.galileo.repository.RoleRepository;
import fr.isika.projet4.galileo.repository.UtilisateurRepository;
import fr.isika.projet4.galileo.security.jwt.JwtProvider;
import fr.isika.projet4.galileo.services.UtilisateurDao;
import fr.isika.projet4.galileo.services.UtilisateurDetailsServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestApis {
	

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UtilisateurRepository utilisateurRepository;
    
    @Autowired
    LivreRepository livreRepository;

    @Autowired
    AuteurRepository auteurRepository;
    
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;
    
    @Autowired
    UtilisateurDetailsServiceImpl udsi;
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /*
     * 
     * Identification d'un utilisateur
     * 
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequete) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                		loginRequete.getUsername(),
                		loginRequete.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        UtilisateurDao userPrincipal = (UtilisateurDao) authentication.getPrincipal();
        
        return ResponseEntity.ok(new JwtReponse(jwt, userPrincipal.getLivres(), userPrincipal.getNom(), userPrincipal.getPrenom(), userDetails.getUsername(), userDetails.getAuthorities()));
    }

    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /*
     * 
     * Ajouter un livre à la liste des livres lus d'un utilisateur
     * 
     */
    @PutMapping("/ajouter/livre")
    public ResponseEntity<String> livreLu(@Valid @RequestBody LivreLu livre) {

    	Utilisateur utilisateur = utilisateurRepository.findByUsername(livre.getUsername()).orElseThrow(() -> 
        new UsernameNotFoundException("Le nom d'utilisateur " + livre.getUsername() + " n'existe pas !") );
    	for(Livre lvr: utilisateur.getLivres()) {
    		
    		if(livre.getMongoId().equalsIgnoreCase(lvr.getMongoId()) ) {
    			
    			return new ResponseEntity<String>("Echec -> Vous avez déjà ajouté ce livre à votre liste de livrs lus !",
                        HttpStatus.BAD_REQUEST);
    			
    		}
    	}
    	
    	Livre livreLu = livreRepository.findByMongoId(livre.getMongoId()).orElse( new Livre(livre.getMongoId()));
    	
    	utilisateur.getLivres().add(livreLu);
    	
    	utilisateurRepository.saveAndFlush(utilisateur);
     
    	return ResponseEntity.ok().body("Livre ajouté avec succès !");
    }
      
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    
    /*
     * 
     * Ajouter un auteur à la liste des auteurs préférés d'un utilisateur
     * 
     */
    @PutMapping("/ajouter/auteur")
    public ResponseEntity<?> monAuteur(@Valid @RequestBody MonAuteur monAuteur) {

    	Utilisateur utilisateur = utilisateurRepository.findByUsername(monAuteur.getUsername()).orElseThrow(() -> 
        new UsernameNotFoundException("Le nom d'utilisateur " + monAuteur.getUsername() + " n'existe pas !") );
    	
    	Auteur auteur = auteurRepository.findByMongoId(monAuteur.getIdAuteur()).orElse(new Auteur(monAuteur.getIdAuteur()));
    	for(Auteur aut: utilisateur.getAuteurs()) {
    		
    		if(monAuteur.getIdAuteur().equalsIgnoreCase(aut.getMongoId())) {
    			
    			return new ResponseEntity<String>("Fail -> Vous avez déjà ajouté cet auteur dans votre liste !",
                        HttpStatus.BAD_REQUEST);
    			
    		}
    	}
    	
    	utilisateur.getAuteurs().add(auteur);
    	utilisateurRepository.saveAndFlush(utilisateur);
     
    	return ResponseEntity.ok().body("Auteur ajouté avec succès !");
    }
     
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

    /*
     * 
     * Inscription d'un utilisateur
     * 
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequete) {
        if(utilisateurRepository.existsByUsername(signUpRequete.getUsername())) {
            return new ResponseEntity<String>("Fail -> Ce nom d'utilisateur est déja utilisé !",
                    HttpStatus.BAD_REQUEST);
        }

        if(utilisateurRepository.existsByEmail(signUpRequete.getEmail())) {
            return new ResponseEntity<String>("Fail -> cet Email est déjà utilisée !",
                    HttpStatus.BAD_REQUEST);
        }

        // Création d'un compte utilisateur
        Utilisateur utilisateur = new Utilisateur(signUpRequete.getNom(), signUpRequete.getPrenom(), signUpRequete.getUsername(),
        		signUpRequete.getEmail(), encoder.encode(signUpRequete.getPassword()));

        Set<String> strRoles = signUpRequete.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
        	switch(role) {
	    		case "admin":
	    			Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
	                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Role Utilisateur non défini."));
	    			roles.add(adminRole);
	    			
	    			break;
	    		case "pm":
	            	Role pmRole = roleRepository.findByName(RoleName.ROLE_PM)
	                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Role Utilisateur non défini."));
	            	roles.add(pmRole);
	            	
	    			break;
	    		default:
	        		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
	                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Role Utilisateur non défini."));
	        		roles.add(userRole);        			
        	}
        });
        
        utilisateur.setRoles(roles);
        utilisateurRepository.save(utilisateur);

        return new ResponseEntity<>(new ResponseMessage("Utilisateur créé avec succès !"), HttpStatus.OK);
    }
    
    
    
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
//    /*
//     * 
//     * Ajouter un livre à la liste générale des livres
//     * 
//     */
//    @PostMapping("/ajouter/livres")
//    public ResponseEntity<String> ajouterLivre(@Valid @RequestBody AjoutLivre livre) {
//        if(livreRepository.existsByMongoId(livre.getMongoId())) {
//            return new ResponseEntity<String>("Fail -> Ce livre existe déjà !",
//                    HttpStatus.BAD_REQUEST);
//        }
//
//        // Création d'un livre
//        Livre monLivre = new Livre(livre.getMongoId());
//
//        
//        livreRepository.save(monLivre);
//
//        return ResponseEntity.ok().body("le livre de id : "+livre.getMongoId()+ " a été créé avec succès !");
//    }
//    
//    
//    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    
//    
//    /*
//     * 
//     * Ajouter un auteur à la liste générale des auteurs
//     * 
//     */
//    @PostMapping("/ajouter/auteurs")
//    public ResponseEntity<String> ajouterAuteur(@Valid @RequestBody AjoutAuteur auteur) {
//        if(auteurRepository.existsByMongoId(auteur.getMongoId())) {
//            return new ResponseEntity<String>("Fail -> Cet auteur existe déjà !",
//                    HttpStatus.BAD_REQUEST);
//        }
//
//        // Création d'un auteur
//        Auteur monAuteur = new Auteur(auteur.getMongoId()); 
//        auteurRepository.save(monAuteur);
//
//        return ResponseEntity.ok().body("Auteur de id : "+auteur.getMongoId()+ " a été créé avec succès !");
//    }
//    
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    
    
    /*
     * 
     * Identification d'un utilisateur
     * 
     */
//    @PostMapping("/livres/lus")
//    public ResponseEntity<?> livresLus(@Valid @RequestBody String utilisateur) {
//        
//    	Utilisateur user = utilisateurRepository.findByUsername(utilisateur).orElse(null);
//        
//    	List<Livre> lus = null;
//    	for(Livre lvr : user.getLivres()) {
//    		
//    		
//    	}
//        
//        return ResponseEntity.ok(lus.toString());
//    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /*
     * 
     * Renvoie la liste des auteurs favoris d'un utilisateur
     * 
     */
    @PostMapping("/mesauteurs")
    public ResponseEntity<?> mesAuteur(@Valid @RequestBody User user) {

    	Utilisateur utilisateur = utilisateurRepository.findByUsername(user.getIdMongo()).orElseThrow(() -> 
        new UsernameNotFoundException("Le nom d'utilisateur " + user.getIdMongo() + " n'existe pas !") );

    	AuteurReponse auteurs = new AuteurReponse();
    	for(Auteur aut: utilisateur.getAuteurs()) {

    	  auteurs.getAuteurs().add(aut.getMongoId());
    	}

     
    	return ResponseEntity.ok().body(auteurs);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /*
     * 
     * Renvoie la liste des livres favoris d'un utilisateur
     * 
     */
    @PostMapping("/meslivres")
    public ResponseEntity<?> meslivre(@Valid @RequestBody User user) {

    	Utilisateur utilisateur = utilisateurRepository.findByUsername(user.getIdMongo()).orElseThrow(() -> 
        new UsernameNotFoundException("Le nom d'utilisateur " + user.getIdMongo() + " n'existe pas !") );

    	List<String> mesLivres = new ArrayList<String>();
    	
    	for(Livre lvr: utilisateur.getLivres()) {

    	  mesLivres.add(lvr.getMongoId());
    	}

     
    	return ResponseEntity.ok().body(mesLivres);
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /*
     * 
     * Supprime un livre de la liste des livre d'un utilisateur
     * 
     */
    @PutMapping("/supprimer-livre")
    public ResponseEntity<?> supprimerLivre(@Valid @RequestBody LivreLu livre) {

    	Utilisateur utilisateur = utilisateurRepository.findByUsername(livre.getUsername()).orElseThrow(() -> 
        new UsernameNotFoundException("Le nom d'utilisateur " + livre.getUsername() + " n'existe pas !") );

        for (Livre lvr : utilisateur.getLivres()) {
        	
        	if(lvr.getMongoId().equalsIgnoreCase(livre.getMongoId())) {
        		
        		utilisateur.getLivres().remove(lvr);
        		
        		utilisateurRepository.saveAndFlush(utilisateur);
        		
        	}
			
		}
     
    	return ResponseEntity.ok().body("Le livre d id : " + livre.getMongoId() + " a bien été supprimé !");
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /*
     * 
     * Supprime un auteur de la liste des auteurs favoris d'un utilisateur
     * 
     */
    @PutMapping("/supprimer-auteur")
    public ResponseEntity<?> supprimerAuteur(@Valid @RequestBody MonAuteur auteur) {

    	Utilisateur utilisateur = utilisateurRepository.findByUsername(auteur.getUsername()).orElseThrow(() -> 
        new UsernameNotFoundException("Le nom d'utilisateur " + auteur.getUsername() + " n'existe pas !") );

        for (Auteur aut : utilisateur.getAuteurs()) {
        	
        	if(aut.getMongoId().equalsIgnoreCase(auteur.getIdAuteur())) {
        		
        		utilisateur.getAuteurs().remove(aut);
        		
        		utilisateurRepository.saveAndFlush(utilisateur);
        		
        	}
			
		}
     
    	return ResponseEntity.ok().body("L auteur d id : " + auteur.getIdAuteur() + " a bien été supprimé !");
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}
